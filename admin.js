// Obtener referencias a los elementos de la tabla y los botones
const tabla = document.querySelector(".tabla-habitaciones tbody");
const botonAgregar = document.querySelector("#agregar-habitacion");
const botonQuitar = document.querySelector("#quitar-habitacion");
// Inicializar el número de habitaciones a 3
let numHabitaciones = 6;
// Agregar los oyentes de eventos para los botones
botonAgregar.addEventListener("click", agregarHabitacion);
botonQuitar.addEventListener("click", quitarHabitacion);
// Función para agregar una fila a la tabla
function agregarHabitacion() {
  // Incrementar el número de habitaciones
  numHabitaciones++;
  // Crear la nueva fila
  const fila = document.createElement("tr");
  fila.innerHTML = `
    <td>${numHabitaciones}0${numHabitaciones}</td>
    <td><input type="text" value="Individual"></td>
    <td><input type="text" value="1"></td>
    <td><input type="text" value="$500/mes"></td>
  `;
  tabla.appendChild(fila);
}
function quitarHabitacion() {		
// Solo quitar una fila si hay más de 3 habitaciones y hay filas que se pueden eliminar
if (numHabitaciones > 3) {
    // Decrementar el número de habitaciones
    numHabitaciones--;
    // Obtener la última fila que se puede eliminar
    const ultimaFila = tabla.lastElementChild;
    // Mostrar una confirmación antes de eliminar la fila
    if (confirm("¿Está seguro que desea eliminar esta fila?")) {
        // Quitar la última fila de la tabla
        tabla.removeChild(ultimaFila);
    }
} else if (numHabitaciones == 2 || numHabitaciones == 1){
    // Mostrar un mensaje de error
    alert("No se puede eliminar esta fila");
}}
const formulario = document.querySelector('#registro-estudiante');
const tablaestudiante = document.querySelector('.tabla-estudiantes tbody');

formulario.addEventListener('submit', (event) => {
event.preventDefault();
const nombre = document.querySelector('#nombre').value;
const email = document.querySelector('#email').value;
const telefono = document.querySelector('#telefono').value;
const habitacion = document.querySelector('#tipo-habitacion').value;
const fila = document.createElement('tr');
fila.innerHTML = `<td>${nombre}</td><td>${email}</td><td>${telefono}</td><td>${habitacion}</td>
    <td><button type="eliminate" class="eliminar-fila" data-message="¿Está seguro que desea eliminar esta fila?">Eliminar</button></td>`;
tablaestudiante.appendChild(fila);
fila.querySelector('.eliminar-fila').addEventListener('click', () => {
const confirmacion = fila.querySelector('.eliminar-fila').getAttribute('data-message');
if (fila.rowIndex <= 2) { // verificar si es una de las dos primeras filas
    alert('No se puede eliminar esta fila.'); // mostrar mensaje de error
} else if (confirm(confirmacion)) {
    fila.remove();
}});
formulario.reset();
formulario.style.display = 'none';
});
function mostrarFormulario() {
formulario.style.display = 'block';}

//para no volver a cargar a cada rato las tablas xml

let datosCargadosXML = false;

// Función para cargar datos desde un archivo XML
function cargarDatosXML() {
  // Verificar si los datos ya se cargaron previamente
  if (datosCargadosXML) {
    // Mostrar una alerta y salir de la función
    alert('Ya se han cargado los datos XML');
    return;
  }

  // Crear una instancia del objeto XMLHttpRequest
  var xhr = new XMLHttpRequest();

  // Configurar la solicitud GET para el archivo XML
  xhr.open("GET", "datos/habitaciones.xml", true);

  // Definir el evento que se ejecutará cuando cambie el estado de la solicitud
  xhr.onreadystatechange = function() {
    // Verificar si la solicitud se ha completado y se han recibido los datos
    if (xhr.readyState === 4) {
      // Verificar si la solicitud se realizó correctamente (estado de respuesta 200)
      if (xhr.status === 200) {
        // Obtener el objeto XML del archivo cargado
        var xml = xhr.responseXML;

        // Obtener todas las filas del XML con la etiqueta "habitacion"
        var filas = xml.getElementsByTagName("habitacion");

        // Obtener una referencia al cuerpo de la tabla en el HTML
        var cuerpoTabla = document.getElementById("habitaciones-tabla-body");

        // Vaciar el contenido existente del cuerpo de la tabla
        cuerpoTabla.innerHTML = "";

        // Iterar sobre cada fila en el XML
        for (var i = 0; i < filas.length; i++) {
          // Obtener la fila actual del XML
          var filaXML = filas[i];

          // Crear un elemento <tr> en el HTML para representar la fila
          var filaHTML = document.createElement("tr");

          // Obtener todas las celdas de la fila actual del XML
          var celdasXML = filaXML.children;

          // Iterar sobre cada celda en el XML
          for (var j = 0; j < celdasXML.length; j++) {
            // Obtener la celda actual del XML
            var celdaXML = celdasXML[j];

            // Crear un elemento <td> en el HTML para representar la celda
            var celdaHTML = document.createElement("td");

            // Establecer el contenido de texto de la celda HTML
            celdaHTML.textContent = celdaXML.textContent;

            // Agregar la celda HTML a la fila HTML
            filaHTML.appendChild(celdaHTML);
          }

          // Agregar la fila HTML al cuerpo de la tabla
          cuerpoTabla.appendChild(filaHTML);
        }

        // Marcar los datos como cargados exitosamente
        datosCargadosXML = true;
      } else {
        // Mostrar una alerta si ocurrió un error al cargar el archivo XML
        alert("Error al cargar el archivo XML");
      }
    }
  };

  // Enviar la solicitud para cargar el archivo XML
  xhr.send();
}

//para no volver a cargar a cada rato las tablas json
let datosCargadosJson = false;

// Función para cargar datos desde un archivo JSON
function cargarDatosJSON() {
  // Verificar si los datos ya se cargaron previamente
  if (datosCargadosJson) {
    // Mostrar una alerta y salir de la función
    alert('Ya se han cargado los datos JSON');
    return;
  }

  // Realizar una solicitud fetch para obtener el archivo JSON
  fetch('datos/estudiantes.json')
    .then(response => response.json()) // Convertir la respuesta en formato JSON
    .then(data => {
      // Llamar a la función mostrarDatos y pasarle los datos JSON
      mostrarDatos(data);

      // Marcar los datos como cargados exitosamente
      datosCargadosJson = true;
    })
    .catch(error => {
      // Mostrar una alerta si ocurrió un error al cargar los datos JSON
      alert('Error al cargar los datos JSON');
    });
}

// Función para mostrar los datos en la tabla
function mostrarDatos(estudiantes) {
  // Obtener una referencia al cuerpo de la tabla en el HTML
  const bodyTablaEstudiantes = document.getElementById('estudiantes-tabla-body');

  // Vaciar el contenido existente del cuerpo de la tabla
  bodyTablaEstudiantes.innerHTML = '';

  // Iterar sobre cada estudiante en el arreglo de estudiantes
  estudiantes.forEach(estudiante => {
    // Crear un elemento <tr> en el HTML para representar la fila
    const fila = document.createElement('tr');

    // Establecer el contenido HTML de la fila con los datos del estudiante
    fila.innerHTML = `
      <td>${estudiante.Nombre}</td>
      <td>${estudiante.Correo}</td>
      <td>${estudiante.Telefono}</td>
      <td>${estudiante.Habitacion}</td>
      <td>${estudiante.Opcion}</td>
    `;

    // Agregar la fila al cuerpo de la tabla
    bodyTablaEstudiantes.appendChild(fila);
  });
}
